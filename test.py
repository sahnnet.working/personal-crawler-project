import asyncio
from storage import MongoStorage
from spider import downloader


async def run1() -> None:
    url = 'https://www.tezolmarket.com/Home/GetProductQueryResult'
    categories = [
        {'CategoryId': 1},
        {'CategoryId': 24},
        {'CategoryId': 41},
        {'CategoryId': 55},
        {'CategoryId': 66},
    ]
    results = await asyncio.gather(
        *[downloader(method='POST', url=url, params=params, response_type='dict') for params in categories])

    mongo_storage = MongoStorage()

    for result in results:
        await mongo_storage.store(result['Products'], 'tezolmarket.com')


async def run2() -> None:
    url = 'https://apigateway.okala.com/api/Search/v1/Product/Search?categorySlugs='
    categorySlugs = [
        'foodstuffs',
        'drinks-herbaltea',
        'grocery',
        'cosmetics-hygiene',
    ]
    results = await asyncio.gather(
        *[downloader(method='GET', url=f'{url}{slug}', response_type='dict') for slug in categorySlugs])

    mongo_storage = MongoStorage()

    for result in results:
        await mongo_storage.store(result['entities'], 'okala.com')


async def main() -> None:
    t1 = asyncio.create_task(run1())
    t2 = asyncio.create_task(run2())

    await t1
    await t2


if __name__ == '__main__':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())
