import aiohttp
from selectolax.parser import HTMLParser


def is_status_ok(status_code: int) -> bool:
    if status_code == 200:
        return True
    elif status_code == 403:
        raise ValueError('Error! Please check your request headers')
    else:
        raise ValueError('Website is down Or you are offline')


async def downloader(method: str, url: str, params=None, headers=None, response_type='bytes') -> bytes | str | dict:
    async with aiohttp.ClientSession() as session:
        async with session.request(method=method, url=url, params=params, headers=headers) as response:
            if is_status_ok(response.status):
                if response_type == 'bytes':
                    result = await response.read()  # Binary Response Content
                elif response_type == 'str':
                    result = await response.text()  # Response Content
                elif response_type == 'dict':
                    result = await response.json()  # JSON Response Content
                else:
                    raise ValueError('The requested response type is incorrect')

    return result


async def parser(page: bytes | str) -> HTMLParser:
    return HTMLParser(page)
