# Personal crawler project

## Table of Content

1. [Install MongoDB](#install-mongodb)
2. [Start mongod Processes](#start-mongod-processes)
3. [Install Libraries](#install-libraries)
4. [Run Test Code](#run-test-code)

___

### Install MongoDB

```
https://www.mongodb.com/docs/manual/installation/
```

### Start mongod Processes

```
https://www.mongodb.com/docs/manual/tutorial/manage-mongodb-processes/#start-mongod-processes
```

### Install Libraries

```
pip install -r requirements.txt 
```

### Run Test Code

```
python test.py
```

Collections are stored in the crawler (database).
