from pymongo import MongoClient


class MongoDatabase:
    instance = None

    @classmethod
    def __new__(cls, *args, **kwargs):
        if cls.instance is None:
            cls.instance = super().__new__(*args, **kwargs)
        return cls.instance

    def __init__(self):
        URL = 'mongodb://localhost:27017'  # Host=localhost , Port=27017
        self.client = MongoClient(URL)
        self.database = self.client['crawler']
